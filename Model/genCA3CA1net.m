function [Time,Xs,ISYN,LFPCA1,LFPCA3,Electr,Nlabels] = genCA3CA1net(TX)
% [...] = genCA3CA1net(...) - Generate simulation of the CA3-CA1 network in
% standard conditions. 
%
% Syntax: [Time,Xs,ISYN,LFPCA1,LFPCA3,Electr,Nlabels] = genCA3CA1net(TX)
%
% Inputs: 
% TX - [DOUBLE] duration of the simulation (in seconds) (e.g. 5)
% 
% Output:
% Time - Time axis
% Xs   - Membrane potentials (Vm) of pyramidal cells and interneurons [Time
%           x Neuron x Compartment(1:soma, 2:dendrite))]
% ISYN - synaptic currents [Time x Neuron x PSC type (:,:,1)-EPSC; (:,:,2)-
%       IPSC; (:,:,3)-dEPSC)
% LFPCA1 / LFPCA3 - LFP signals of CA [Time x Channel]
% Electr        - depth of electrode tips [um]
% Nlabels       - neuron labels (1: CA3 Pyr, 2: CA3 Int, 3: CA1 Pyr, 4: CA1 Int)
%
% JF Ramirez-Villegas (c); MPI for Biol. Cybern.
%
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



if nargin<1
    help genCA3CA1net
    error('Not enough input arguments.\n')
end

TX = TX*1000;

%Connectivity in the network
sigmapy         = 1; %Sigma pyramidal neurons (mm)
Ncells          = 150;  %Number of cells 
IntCellDist     = 0.01; %Inter-neuron distance (mm)

IntProp         = 0.1; % proportion of interneurons 
sigma2          = 0.1; %Sigma interneurons (mm) 
sigma_ca3ca1    = 1.2; %Sigma pyramidal neurons (mm)
arraydist       = 1000e-3; %CA3-CA1 distance 

% CA1
[centers2,A2,ntype2] = evest_syn_connect(Ncells,IntCellDist,sigmapy, ...
    'twoclass',IntProp,'sigma2',sigma2,'centers','linear', ...
    'penalty',[0 0.05]);  
% CA3
[centers1,A1,ntype1] = evest_syn_connect(Ncells,IntCellDist,sigmapy, ...
    'twoclass',IntProp,'sigma2',sigma2,'centers','linear','uniform',4*sigma2,... 
    'penalty',[0 0]);

% CA3-CA1 connectivity
[A_ca3ca1] = CA1_CA3Connections(centers1,centers2,arraydist,sigma_ca3ca1);

% Simulate network
CI          = zeros(1,42);  %initial conditions
DT          = 0.02;         %sampling period (in ms)
NlabelsCA3  = ntype1;       %neuron types and connectivity matrices
NlabelsCA1  = ntype2;
A_CA3CA1    = A_ca3ca1;   
A_CA3       = A1;
A_CA1       = A2;

% Define burn-in duration in ms
TLIM        = 500;

[Time,Xs,Xd,ITOT,ISYN] = simCA3CA1Network(TX+TLIM,CI,DT,NlabelsCA3, ...
    NlabelsCA1,A_CA3,A_CA1,A_CA3CA1);

Nlabels = [NlabelsCA3,NlabelsCA1+2];

%Get rid of the transient response up to 500 ms of integration time
tpos        = Time>=TLIM;
Time        = Time(tpos) - TLIM;
Xs          = Xs(:,tpos);
Xd          = Xd(:,tpos);
ITOT        = ITOT(:,tpos,:); 
ISYN        = ISYN(:,tpos,:);

% differentiate integrated synaptic current ouput of the simulation
ISYN = cat(2,zeros(size(ISYN,1),1,size(ISYN,3)),diff(ISYN,1,2));

% concatenate compartments
Xs(:,:,2) = Xd;

% Compute the field potentials 
cm   = [3 0.75 3 0.75]; %Capacitances (capacitive current)
for k=1:size(ITOT,3)
    for j=1:size(ITOT,1)
    sTemp = [0,diff([Xs(j,:,k).*cm(Nlabels(j))])]; %capacitive
    sTemp2 = [0,diff([ITOT(j,:,k).*cm(Nlabels(j))])]; %spiking / synaptic
    ITOT(j,:,k) = [(sTemp2 + sTemp)];
    end
end
% CA1 cells LFP
Itrans = ITOT(Nlabels==3 | Nlabels==4,:,:);
STRTick     = 100;
nlabel      = Nlabels(Nlabels==1 | Nlabels==2); 
[Electr,LFPCA1] = compelectrode_two(Itrans(:,:,1),Itrans(:,:,2),nlabel, ...
    'ypos',STRTick);
Electr = squeeze(Electr); 

% CA3 cells LFP
Itrans = ITOT(Nlabels==1 | Nlabels==2,:,:);
STRTick     = 100;
nlabel      = Nlabels(Nlabels==1 | Nlabels==2); 
[~,LFPCA3] = compelectrode_two(Itrans(:,:,1),Itrans(:,:,2),nlabel, ...
    'ypos',STRTick); 

%update dimension to put time first
Xs = permute(Xs,[2,1,3]);
ISYN = permute(ISYN,[2,1,3]);
LFPCA1 = permute(LFPCA1,[2,1]);
LFPCA3 = permute(LFPCA3,[2,1]);
Time = Time(:);

% dereference memberane potential
Xs = Xs - 60;

% Basic plots
close all,
figure(1)
TypeLegend = {'CA3 Pyr.','CA3 Int.','CA1 Pyr.','CA1 Int.'};
for kType=1:4
    subplot(2,4,kType)
    plot(Time,squeeze(mean(ISYN(:,Nlabels==kType,:),2)))
    if kType ==1,
        ylabel('Population average synaptic currents'),
    end
    title(TypeLegend{kType})
    if kType ==4,
    legend('sEPSC','sIPSC','dEPSC')
    end
    subplot(2,4,4+kType)
    plot(Time,squeeze(mean(Xs(:,Nlabels==kType,1),3)))
    if kType ==1,
        ylabel('Somatic membrane potentials'),
        xlabel('Time (ms)')
    end
end




