function [Kint0,Itot,Itotd,IAMPA,IGABA,IAMPAd] = simRB_CA3Networklfp(XHpy,Nlabels,A,DT,varargin)
%This function computes the K's for the Wang-Buzsaki model
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Esyn  = [-15 60]; 
for karg=1:2:length(varargin)
    switch lower(varargin{karg})
        case 'esyn'
            Esyn = varargin{karg+1};
    end
end

XHpy0 = XHpy(:,12:21);
N = length(nonzeros(Nlabels==2));
Kint0 = zeros(N,1,4);

%Pick only pyramidal cells 
Intind = (Nlabels==2);
Pyrind = (Nlabels==1);
XHall = zeros(size(XHpy,1),1);
XHall(Pyrind) = XHpy(Pyrind,1); 
XHall(Intind) = XHpy(Intind,12); 

%Model parameters (maximal conductances in mS/cm^2)
gL      = 0.1;       
gNa     = 100;
gK_DR   = 135; 
gCa     = 1; 
gK_C    = 8; 
gsyn    = 0.01;     %Maximal synaptic conductance

Is      = 0;         %Applied current to the soma
Id      = 0;         %Applied current to the dendrite
gc      = 2.1;       %Coupling conductance (mS/cm^2) - 2.1
p       = 0.5;        %Proportion of cell area taken by the soma
Cm      = 0.75;      %Membrane capacitance (uF/cm^2)

%Reversal potentials (all in mV)
VNa     = 115; 
VCa     = 140; 
VK      = -25;
VL      = -0;
tausyn  = 5; %Tau for synapses (ms)
tausynA = 2;

%Compute all gating variables-----------------------------------------%
alpham = 0.32*(13.1 - XHpy0(Intind,1))./(exp((13.1 - XHpy0(Intind,1))/4) - 1);
betam = 0.28*(XHpy0(Intind,1) - 40.1)./(exp((XHpy0(Intind,1) - 40.1)/5) - 1);
Minf = alpham./(alpham + betam);

alphan = 0.016*(35.1 - XHpy0(Intind,1))./(exp((35.1 - XHpy0(Intind,1))/5) - 1);
betan = 0.25*exp(0.5 - 0.025*XHpy0(Intind,1));
Ninf = alphan./(alphan + betan);
Taun = 1./(alphan + betan);

alphah = 0.128.*exp((17 - XHpy0(Intind,1))/18); %XH(1) is potential in soma (mV)
betah = 4./(1 + exp((40 - XHpy0(Intind,1))/5));
Hinf = alphah./(alphah + betah);
Tauh = 1./(alphah + betah);

alphas = 1.6./(1 + exp(-0.072*(XHpy0(Intind,2) - 65))); %XH(2) is potential in dendrites (mV)
betas = 0.02*(XHpy0(Intind,2) - 51.1)./(exp((XHpy0(Intind,2) - 51.1)/5) - 1);
Sinf = alphas./(alphas + betas);
Taus = 1./(alphas + betas);

cx = (exp((XHpy0(Intind,2) - 10)/11 - (XHpy0(Intind,2) - 6.5)/27))./18.975;
cx1 = 2*exp((6.5 - XHpy0(Intind,2))/27);
alphac = (cx.*(XHpy0(Intind,2)<=50) + cx1.*(XHpy0(Intind,2)>50));
bx = 2*exp((6.5 - XHpy0(Intind,2))/27) - alphac;
betac = bx.*(XHpy0(Intind,2)<=50);
Cinf = alphac./(alphac + betac);
Tauc = 1./(alphac + betac);
%End gating variables ------------------------------------------------%
        
%Compute ionic currents ----------------------------------------------%
Ileaks = gL*(XHpy0(Intind,1) - VL);
Ileakd = gL*(XHpy0(Intind,2) - VL);
INa = gNa*(Minf.^2).*XHpy0(Intind,3).*(XHpy0(Intind,1) - VNa); %h = XH(3)
IK_DR = gK_DR*(XHpy0(Intind,4).^3).*(XHpy0(Intind,1) - VK); %n = XH(4)
ICa = gCa*(XHpy0(Intind,5).^2).*(XHpy0(Intind,2) - VCa); %s = XH(5)
XCA = min(XHpy0(Intind,7)./250,1); 
IK_C = gK_C*XHpy0(Intind,6).*XCA.*(XHpy0(Intind,2) - VK); % c = XH(6); Ca = XH(7);

%Compute synaptic currents
[IsynsI,IsynsE,SMnmdasEx,SMnmdasInh] = sub_syncurrentp1(XHall,XHpy0(Intind,1), ...
XHpy0(Intind,8),XHpy0(Intind,9),A(:,Intind),gsyn,Esyn,Nlabels);

[Isynd,SMnmdad] = sub_syncurrentd1(XHall,XHpy0(Intind,2), ...
XHpy0(Intind,10),A(:,Intind),gsyn,Esyn,Nlabels);
%---------------------------------------------------------------------%
%Compute main differential equations ---------------------------------%
%Somatic and dendritic compartments
Kint0(:,1,1) = (DT/Cm)*(-INa - IK_DR - Ileaks - (IsynsE(:) + IsynsI(:))./p + (gc./p).*(XHpy0(Intind,2) - ...
    XHpy0(Intind,1)) + Is/p); 
Itot(:,1) = (DT/Cm)*(INa + IK_DR + Ileaks + (IsynsE(:) + IsynsI(:))./p);
IAMPA(:,1) = (DT)*(- (IsynsE)./p); 
IGABA(:,1) = (DT)*(- (IsynsI)./p);

Kint0(:,1,2) = (DT/Cm)*(-ICa - IK_C - Ileakd - Isynd(:)./(1-p) - (gc./(1-p)).*(XHpy0(Intind,2) - ...
    XHpy0(Intind,1)) + Id/(1-p)); 
Itotd(:,1) = (DT/Cm)*(ICa + IK_C + Ileakd + Isynd(:)./(1-p));
IAMPAd(:,1) = (DT)*(- (Isynd)./p); 

%Gating variables
Kint0(:,1,3) = DT*((Hinf - XHpy0(Intind,3))./Tauh);
Kint0(:,1,4) = DT*((Ninf - XHpy0(Intind,4))./Taun);
Kint0(:,1,5) = DT*((Sinf - XHpy0(Intind,5))./Taus);
Kint0(:,1,6) = DT*((Cinf - XHpy0(Intind,6))./Tauc);
Kint0(:,1,7) = DT*(-0.13*ICa - 0.075*XHpy0(Intind,7));

%Synaptic currents
Kint0(:,1,8) = DT*(SMnmdasInh(:) - XHpy0(Intind,8)./tausyn);
Kint0(:,1,9) = DT*(SMnmdasEx(:) - XHpy0(Intind,9)./tausynA);
Kint0(:,1,10) = DT*(SMnmdad(:) - XHpy0(Intind,10)./tausynA);

function [IsynInh,IsynExc,SMnmdaExc,SMnmdaInh] = sub_syncurrentp1(XH,XH2,XH3,XH4,A,gsyn,vsyn,Nlabels)
%This function computes the synaptic current over the dendritic compartment
%of a postsynaptic pyramidal cell. 

Xlab = zeros(length(Nlabels),1);
Xlab(Nlabels==1) = 1;
Xlab1 = zeros(length(Nlabels),1);
Xlab1(Nlabels==2) = 1;

alphax = zeros(length(Nlabels),1);
alphax(Nlabels==1) = 1.25;     
alphax(Nlabels==2) = 20;       
alphax = repmat(alphax,[1,size(A,2)]);

Fpre = repmat(XH(Nlabels==1 | Nlabels==2),[1,size(A,2)]); 
Flab = repmat(Xlab(Nlabels==1 | Nlabels==2),[1,size(A,2)]);
Flabinh = repmat(Xlab1(Nlabels==1 | Nlabels==2),[1,size(A,2)]);

%Here, with Nlabels, choose who is actually pre-synaptic(!!) - Excitation
SMnmdaExc = Flab(Nlabels==1,:).*(Fpre(Nlabels==1,:).*A(Nlabels==1,:) >= 20); %select cells according to adj matrix and sum-up (18)
SMnmdaExc = sum(alphax(Nlabels==1,:).*SMnmdaExc,1);

%Here, with Nlabels, choose who is actually pre-synaptic(!!) - Inhibition
SMnmdaInh = Flabinh(Nlabels==2,:).*(Fpre(Nlabels==2,:).*A(Nlabels==2,:) >= 20); %select cells according to adj matrix and sum-up (18)
SMnmdaInh = sum(alphax(Nlabels==2,:).*SMnmdaInh,1);

IsynInh = gsyn*XH3.*(XH2 - vsyn(1));
IsynExc = gsyn*XH4.*(XH2 - vsyn(2));

function [Isyn,SMnmda] = sub_syncurrentd1(XH,XH2,XH3,A,gsyn,vsyn,Nlabels)
%This function computes the synaptic current over the dendritic compartment
%of a postsynaptic pyramidal cell

Xlab = zeros(length(Nlabels),1);
Xlab(Nlabels==1) = 0;
Xlab(Nlabels==2) = 0;

alphax = zeros(length(Nlabels),1);
alphax(Nlabels==1) = 0; %for CA3: 0
alphax(Nlabels==2) = 0; %for CA3: 0
alphax = repmat(alphax,[1,size(A,2)]);

Fpre = repmat(XH(Nlabels==1 | Nlabels==2),[1,size(A,2)]); 
Flab = repmat(Xlab(Nlabels==1 | Nlabels==2),[1,size(A,2)]);
SMnmda = Flab.*(Fpre.*A >= 20); %select cells according to adj matrix and sum-up (18)
SMnmda = sum(alphax.*SMnmda,1);
Isyn = gsyn*XH3.*(XH2 - vsyn(2)); %only excitation here