function [Kpyr0,Itot,Itotd,IAMPA,IGABA,IAMPAd] = simPR_CA1Networklfp(XHpy,Nlabels,NlabelsCA1, ...
    A_CA1,A_CA3CA1,DT,varargin)
%This function computes the 4th-order RK K's for the Pinsky-Rinzel model
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


gc = 2.1;      %Coupling conductance (mS/cm^2) - 2.1
gL = 0.1;
VL = 0; 
VSyn = [-15 60]; 
Is = 0.0;
Id = 0.0;
for karg=1:2:length(varargin)
    switch lower(varargin{karg})
        case 'somatic'
            Is = varargin{karg+1};
        case 'coupling'
            gc = varargin{karg+1};
        case 'leak_g' %maximum leak conductance
            gL = varargin{karg+1};
        case 'leak_e' %reversal potential
            VL = varargin{karg+1};
        case 'esyn'
            VSyn = varargin{karg+1};
    end
end

XHpy0 = XHpy(:,22:32);
N = length(nonzeros(Nlabels==3));
Kpyr0 = zeros(N,1,11);

%Pick only pyramidal cells 
pyrind = (Nlabels==3);
pyrind1 = NlabelsCA1==1;

%Default array of all neurons to compute syntaptic currents
Intindca3 = (Nlabels==2);
pyrindca3 = (Nlabels==1);
Intindca1 = (Nlabels==4);
pyrindca1 = (Nlabels==3);
XHall = zeros(size(XHpy,1),1);
XHall(pyrindca3) = XHpy(pyrindca3,1);  %CA3 pyramidal cells (soma)
XHall(Intindca3) = XHpy(Intindca3,12); %CA3 interneurons (soma)
XHall(pyrindca1) = XHpy(pyrindca1,22); %CA1 pyramidal cells (soma)
XHall(Intindca1) = XHpy(Intindca1,33); %CA1 interneurons (soma)

%Model parameters (maximal conductances in mS/cm^2)     
gNa         = 30;
gK_DR       = 15;
gCa         = 10; %7 for CA3/CA1 simulation
gK_AHP      = 0.8;
gK_C        = 15;
gsyn        = 0.01;        %Maximal conductance (uS) - synaptic
p           = 0.5;        %Proportion of cell area taken by the soma
Cm          = 3;         %Capacitance (uF/cm^2)

%Reversal potentials (all in mV)
VNa         = 120;
VCa         = 140;
VK          = -15;
tausyn      = 5; %Tau for synapses (ms) OR: 5 ms
tausynA     = 2; % OR: 2 ms

%Compute all gating variables-----------------------------------------%
alpham = 0.32*(13.1 - XHpy0(pyrind,1))./(exp((13.1 - XHpy0(pyrind,1))/4) - 1);
betam = 0.28*(XHpy0(pyrind,1) - 40.1)./(exp((XHpy0(pyrind,1) - 40.1)/5) - 1);
Minf = alpham./(alpham + betam);

alphan = 0.016*(35.1 - XHpy0(pyrind,1))./(exp((35.1 - XHpy0(pyrind,1))/5) - 1);
betan = 0.25.*exp(0.5 - 0.025.*XHpy0(pyrind,1));
Ninf = alphan./(alphan + betan);
Taun = 1./(alphan + betan);
    
alphah = 0.128*exp((17 - XHpy0(pyrind,1))./18); %XH(1) is potential in soma (mV)
betah = 4./(1 + exp((40 - XHpy0(pyrind,1))./5));
Hinf = alphah./(alphah + betah);
Tauh = 1./(alphah + betah);
    
alphas = 1.6./(1 + exp(-0.072*(XHpy0(pyrind,2) - 65))); %XH(2) is potential in dendrites (mV)
betas = 0.02*(XHpy0(pyrind,2) - 51.1)./(exp((XHpy0(pyrind,2) - 51.1)./5) - 1);
Sinf = alphas./(alphas + betas);
Taus = 1./(alphas + betas);
    
cx = (exp((XHpy0(pyrind,2) - 10)./11 - (XHpy0(pyrind,2) - 6.5)./27))./18.975;
cx1 = 2*exp((6.5 - XHpy0(pyrind,2))/27);
alphac = (cx.*(XHpy0(pyrind,2)<=50) + cx1.*(XHpy0(pyrind,2)>50));
bx = 2*exp((6.5 - XHpy0(pyrind,2))./27) - alphac;
betac = bx.*(XHpy0(pyrind,2)<=50);
Cinf = alphac./(alphac + betac);
Tauc = 1./(alphac + betac);
    
alphaq = min(0.00002.*XHpy0(pyrind,8),0.01); % Ca = XH(8) (see below)
betaq = 0.001;
Qinf = alphaq./(alphaq + betaq);
Tauq = 1./(alphaq + betaq);
%End gating variables ------------------------------------------------%
        
%Compute ionic currents ----------------------------------------------%
Ileaks = gL.*(XHpy0(pyrind,1) - VL);
Ileakd = gL.*(XHpy0(pyrind,2) - VL);
INa = gNa*(Minf.^2).*XHpy0(pyrind,3).*(XHpy0(pyrind,1) - VNa); %h = XH(3)
IK_DR = gK_DR*XHpy0(pyrind,4).*(XHpy0(pyrind,1) - VK); %n = XH(4)
ICa = gCa*(XHpy0(pyrind,5).^2).*(XHpy0(pyrind,2) - VCa); %s = XH(5)
IK_AHP = gK_AHP*XHpy0(pyrind,6).*(XHpy0(pyrind,2) - VK); % q = XH(6)
XCA = min(XHpy0(pyrind,8)./250,1); 
IK_C = gK_C*XHpy0(pyrind,7).*XCA.*(XHpy0(pyrind,2) - VK); % c = XH(7); Ca = XH(8);

%Compute synaptic currents
[IsynI,IsynE,SMnmdaEx,SMnmdaInh] = sub_syncurrentpca1(XHall,XHpy0(pyrind,1), ...
    XHpy0(pyrind,9),XHpy0(pyrind,10),A_CA1(:,pyrind1),A_CA3CA1(:,pyrind1),gsyn,VSyn,Nlabels);

[ISynd,SMnmdaExd] = sub_syncurrentdca1(XHall,XHpy0(pyrind,2), ...
    XHpy0(pyrind,11),A_CA3CA1(:,pyrind1),gsyn,VSyn,Nlabels);
%---------------------------------------------------------------------%
    
%Compute main differential equations ---------------------------------%
%Somatic and dendritic compartment
Kpyr0(:,1,1) = (DT/Cm)*(-Ileaks - INa - IK_DR - (IsynI + IsynE)./p + (gc./p).*(XHpy0(pyrind,2) - ...
    XHpy0(pyrind,1)) + Is./p); 
Itot(:,1) = (DT/Cm)*(Ileaks + INa + IK_DR + (IsynI + IsynE)./p); 
IAMPA(:,1) = (DT)*(- (IsynE)./p); 
IGABA(:,1) = (DT)*(- (IsynI)./p); 

Kpyr0(:,1,2) = (DT/Cm)*(-Ileakd - ICa - IK_AHP - IK_C - ISynd./(1-p) + ...
    (gc./(1-p)).*(XHpy0(pyrind,1) - XHpy0(pyrind,2)) + Id./(1-p));
Itotd(:,1) = (DT/Cm)*(Ileakd + ICa + IK_AHP + IK_C + ISynd./(1-p)); 
IAMPAd(:,1) = (DT)*(- (ISynd)./p); 

%Gating variables
Kpyr0(:,1,3) = DT*((Hinf - XHpy0(pyrind,3))./Tauh);
Kpyr0(:,1,4) = DT*((Ninf - XHpy0(pyrind,4))./Taun);
Kpyr0(:,1,5) = DT*((Sinf - XHpy0(pyrind,5))./Taus);
Kpyr0(:,1,6) = DT*((Qinf - XHpy0(pyrind,6))./Tauq);
Kpyr0(:,1,7) = DT*((Cinf - XHpy0(pyrind,7))./Tauc);
Kpyr0(:,1,8) = DT*(-0.13*ICa - 0.075*XHpy0(pyrind,8));
Kpyr0(:,1,9) = DT*(SMnmdaInh(:) - XHpy0(pyrind,9)./tausyn);
Kpyr0(:,1,10) = DT*(SMnmdaEx(:) - XHpy0(pyrind,10)./tausynA);
Kpyr0(:,1,11) = DT*(SMnmdaExd(:) - XHpy0(pyrind,11)./tausynA);

function [IsynInh,IsynExc,SMnmdaExc,SMnmdaInh] = sub_syncurrentpca1(XH,XH2,XH3,XH4,A_CA1,A_CA3CA1, ...
    gsyn,vsyn,Nlabels)
%This function computes the synaptic current over the dendritic compartment
%of a postsynaptic pyramidal cell. 
%               _______________________________
%              |                               |
%      P       |                               |
%      R       |                               |
%      E       |                               |
%      -       |                               |
%      S       |                               |
%      Y       |                               |
%      N       |                               |
%      A       |                               |
%      P       |                               |
%      T       |                               |
%      I        _______________________________
%      C
%                   P O S T - S Y N A P T I C 
%
%       Therefore: SUM(A,1) is the number of synaptic contacts of each
%       cell. Of course, A(j,j) = 0.
%

Nlabelsx = Nlabels(Nlabels==3 | Nlabels==4);
Xlab = zeros(length(Nlabels),1);
Xlab(Nlabels==3) = 1;
Xlab1 = zeros(length(Nlabels),1);
Xlab1(Nlabels==4) = 1;

%Set alphas for interactions to pyramidal neurons (CA1)
alphax = zeros(length(Nlabels),1);
alphax(Nlabels==1)      = 0; %Pyramidal neurons (CA3) 
alphax(Nlabels==2)      = 0; %Interneurons (CA3)
alphax(Nlabels==3)      = 0; %Pyramidal neurons (CA1)
alphax(Nlabels==4)      = 60;%Interneurons (CA1)

%Compute contribution of CA1
Fpre = repmat(XH(Nlabels==3 | Nlabels==4),[1,size(A_CA1,2)]); 
Flab = repmat(Xlab(Nlabels==3 | Nlabels==4),[1,size(A_CA1,2)]);
Flabinh = repmat(Xlab1(Nlabels==3 | Nlabels==4),[1,size(A_CA1,2)]);
Falph = repmat(alphax(Nlabels==3 | Nlabels==4),[1,size(A_CA1,2)]); 

%Here, with Nlabels, choose who is actually pre-synaptic(!!) - Excitation
SMnmdaExc = Flab(Nlabelsx==3,:).*(Fpre(Nlabelsx==3,:).*A_CA1(Nlabelsx==3,:) >= 20); %select cells according to adj matrix and sum-up (18)
SMnmdaExc = sum(Falph(Nlabelsx==3,:).*SMnmdaExc,1);

%Here, with Nlabels, choose who is actually pre-synaptic(!!) - Inhibition
SMnmdaInh = Flabinh(Nlabelsx==4,:).*(Fpre(Nlabelsx==4,:).*A_CA1(Nlabelsx==4,:) >= 20); %select cells according to adj matrix and sum-up (18)
SMnmdaInh = sum(Falph(Nlabelsx==4,:).*SMnmdaInh,1);

IsynInh = gsyn*XH3.*(XH2 - vsyn(1));
IsynExc = gsyn*XH4.*(XH2 - vsyn(2));

function [IsynExc,SMnmdaExc3] = sub_syncurrentdca1(XH,XH2,XH3,A_CA3CA1,gsyn,vsyn,Nlabels)
%This function computes the synaptic current over the dendritic compartment
%of a postsynaptic pyramidal cell
%               _______________________________
%              |                               |
%      P       |                               |
%      R       |                               |
%      E       |                               |
%      -       |                               |
%      S       |                               |
%      Y       |                               |
%      N       |                               |
%      A       |                               |
%      P       |                               |
%      T       |                               |
%      I        _______________________________
%      C
%                   P O S T - S Y N A P T I C 
%
%       Therefore: SUM(A,1) is the number of synaptic contacts of each
%       cell. Of course, A(j,j) = 0.
%

Xlab = zeros(length(Nlabels),1);
Xlab(Nlabels==1) = 1;

%Contribution of CA3 (Schaffer pathway)
alphax = zeros(length(Nlabels),1);
alphax(Nlabels==1)  = 2; %Pyramidal neurons (CA3)
alphax(Nlabels==2) = 0; %Interneurons (CA3)
alphax(Nlabels==3) = 0; %Pyramidal neurons (CA1)
alphax(Nlabels==4) = 0; %Interneurons (CA1)

Fpre = repmat(XH(Nlabels==1 | Nlabels==2),[1,size(A_CA3CA1,2)]); 
Flab = repmat(Xlab(Nlabels==1 | Nlabels==2),[1,size(A_CA3CA1,2)]);
Falph = repmat(alphax(Nlabels==1 | Nlabels==2),[1,size(A_CA3CA1,2)]);

%Here, with Nlabels, choose who is actually pre-synaptic(!!) - Excitation
SMnmdaExc3 = Flab(Nlabels==1,:).*(Fpre(Nlabels==1,:).*A_CA3CA1(Nlabels==1,:) >= 20); %select cells according to adj matrix and sum-up
SMnmdaExc3 = sum(Falph(Nlabels==1,:).*SMnmdaExc3,1);

IsynExc = gsyn*XH3.*(XH2 - vsyn(2)); %only excitation here