function [Kpyr0,Itot,Itotd,IAMPA,IGABA,IAMPAd] = simPR_CA3Networklfp(XHpy,Nlabels,A,DT,varargin)
%This function computes the 4th-order RK K's for the Pinsky-Rinzel model
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Is      = 0;        %Applied current to the soma 1.5
Id      = 0;        %Applied current to the dendrites
gc      = 2.1;      %Coupling conductance (mS/cm^2) - 2.1
gL      = 0.1;      %Max. leak conductance
VL      = 0;        %Leak reversal potential
VSyn    = [-15 60];        %Synaptic reversal potential

for karg=1:2:length(varargin)
    switch lower(varargin{karg})
        case 'somatic'
            Is = varargin{karg+1};
        case 'dendritic'
            Id = varargin{karg+1};
        case 'coupling'
            gc = varargin{karg+1};
        case 'leak_g' %maximum leak conductance
            gL = varargin{karg+1};
        case 'leak_e' %reversal potential
            VL = varargin{karg+1};
        case 'esyn'
            VSyn = varargin{karg+1};
    end
end

XHpy0 = XHpy(:,1:11);
N = length(nonzeros(Nlabels==1));
Kpyr0 = zeros(N,1,11);

%Pick only pyramidal cells 
Intind = (Nlabels==2);
pyrind = (Nlabels==1);
XHall = zeros(size(XHpy,1),1);
XHall(pyrind) = XHpy(pyrind,1); 
XHall(Intind) = XHpy(Intind,12); 

XHalld = zeros(size(XHpy,1),1);
XHalld(pyrind) = XHpy(pyrind,2); 
XHalld(Intind) = XHpy(Intind,13); 

%Model parameters (maximal conductances in mS/cm^2)      
gNa     = 30;
gK_DR   = 15; 
gCa     = 10;
gK_AHP  = 0.8;
gK_C    = 15; 
gsyn    = 0.005;       %Maximal conductance (nS) - synaptic
p       = 0.5;        %Proportion of cell area taken by the soma
Cm      = 3;         %Capacitance (uF/cm^2)

%Reversal potentials (all in mV)
VNa         = 120;
VCa         = 140;
VK          = -15;
tausyn      = 5; %(GABA-A)
tausynA     = 2; %(AMPA)

%Compute all gating variables-----------------------------------------%
alpham = 0.32*(13.1 - XHpy0(pyrind,1))./(exp((13.1 - XHpy0(pyrind,1))/4) - 1);
betam = 0.28*(XHpy0(pyrind,1) - 40.1)./(exp((XHpy0(pyrind,1) - 40.1)/5) - 1);
Minf = alpham./(alpham + betam);

alphan = 0.016*(35.1 - XHpy0(pyrind,1))./(exp((35.1 - XHpy0(pyrind,1))/5) - 1);
betan = 0.25.*exp(0.5 - 0.025.*XHpy0(pyrind,1));
Ninf = alphan./(alphan + betan);
Taun = 1./(alphan + betan);
    
alphah = 0.128*exp((17 - XHpy0(pyrind,1))./18); %XH(1) is potential in soma (mV)
betah = 4./(1 + exp((40 - XHpy0(pyrind,1))./5));
Hinf = alphah./(alphah + betah);
Tauh = 1./(alphah + betah);
    
alphas = 1.6./(1 + exp(-0.072*(XHpy0(pyrind,2) - 65))); %XH(2) is potential in dendrites (mV)
betas = 0.02*(XHpy0(pyrind,2) - 51.1)./(exp((XHpy0(pyrind,2) - 51.1)./5) - 1);
Sinf = alphas./(alphas + betas);
Taus = 1./(alphas + betas);
    
cx = (exp((XHpy0(pyrind,2) - 10)./11 - (XHpy0(pyrind,2) - 6.5)./27))./18.975;
cx1 = 2*exp((6.5 - XHpy0(pyrind,2))/27);
alphac = (cx.*(XHpy0(pyrind,2)<=50) + cx1.*(XHpy0(pyrind,2)>50));
bx = 2*exp((6.5 - XHpy0(pyrind,2))./27) - alphac;
betac = bx.*(XHpy0(pyrind,2)<=50);
Cinf = alphac./(alphac + betac);
Tauc = 1./(alphac + betac);
    
alphaq = min(0.00002.*XHpy0(pyrind,8),0.01); % Ca = XH(8) (see below)
betaq = 0.001;
Qinf = alphaq./(alphaq + betaq);
Tauq = 1./(alphaq + betaq);
%End gating variables ------------------------------------------------%
        
%Compute ionic currents ----------------------------------------------%
Ileaks = gL.*(XHpy0(pyrind,1) - VL);
Ileakd = gL.*(XHpy0(pyrind,2) - VL);
INa = gNa*(Minf.^2).*XHpy0(pyrind,3).*(XHpy0(pyrind,1) - VNa); %h = XH(3)
IK_DR = gK_DR*XHpy0(pyrind,4).*(XHpy0(pyrind,1) - VK); %n = XH(4)
ICa = gCa*(XHpy0(pyrind,5).^2).*(XHpy0(pyrind,2) - VCa); %s = XH(5)
IK_AHP = gK_AHP*XHpy0(pyrind,6).*(XHpy0(pyrind,2) - VK); % q = XH(6)
XCA = min(XHpy0(pyrind,8)./250,1); 
IK_C = gK_C*XHpy0(pyrind,7).*XCA.*(XHpy0(pyrind,2) - VK); % c = XH(7); Ca = XH(8);

%Synaptic currents
[ISynEx,ISynInh,SMnmdaEx,SMnmdaInh] = sub_syncurrentpca3(XHall,XHpy0(pyrind,1), ...
    XHpy0(pyrind,9), XHpy0(pyrind,10),A(:,pyrind),gsyn,VSyn,Nlabels);

[ISynd,SMnmda1] = sub_syncurrentdca3(XHalld,XHpy0(pyrind,2), ...
    XHpy0(pyrind,11),A(:,pyrind),gsyn,VSyn,Nlabels);
%---------------------------------------------------------------------%
    
%Compute main differential equations ---------------------------------%
%Somatic and dendritic compartment
Kpyr0(:,1,1) = (DT/Cm)*(-Ileaks - INa - IK_DR - (ISynEx + ISynInh)./p + (gc./p).*(XHpy0(pyrind,2) - ...
    XHpy0(pyrind,1)) + Is./p); 
Itot(:,1) = (DT/Cm)*(Ileaks + INa + IK_DR + (ISynEx + ISynInh)./p + Is./p); 
IAMPA(:,1) = (DT)*(- (ISynEx)./p); 
IGABA(:,1) = (DT)*(- (ISynInh)./p); 

Kpyr0(:,1,2) = (DT/Cm)*(-Ileakd - ICa - IK_AHP - IK_C - ISynd./(1-p) + ...
    (gc./(1-p)).*(XHpy0(pyrind,1) - XHpy0(pyrind,2)) + Id./(1-p));
Itotd(:,1) = (DT/Cm)*(Ileakd + ICa + IK_AHP + IK_C + ISynd./(1-p)); 
IAMPAd(:,1) = (DT)*(- (ISynd)./p); 

%Gating variables
Kpyr0(:,1,3) = DT*((Hinf - XHpy0(pyrind,3))./Tauh);
Kpyr0(:,1,4) = DT*((Ninf - XHpy0(pyrind,4))./Taun);
Kpyr0(:,1,5) = DT*((Sinf - XHpy0(pyrind,5))./Taus);
Kpyr0(:,1,6) = DT*((Qinf - XHpy0(pyrind,6))./Tauq);
Kpyr0(:,1,7) = DT*((Cinf - XHpy0(pyrind,7))./Tauc);
Kpyr0(:,1,8) = DT*(-0.13*ICa - 0.075*XHpy0(pyrind,8));
Kpyr0(:,1,9) = DT*(SMnmdaInh(:) - XHpy0(pyrind,9)./tausyn);
Kpyr0(:,1,10) = DT*(SMnmdaEx(:) - XHpy0(pyrind,10)./tausynA);
Kpyr0(:,1,11) = DT*(SMnmda1(:) - XHpy0(pyrind,11)./tausynA);

function [IsynExc,IsynInh,SMnmdaExc,SMnmdaInh] = sub_syncurrentpca3(XH,XH2,XH3,XH4,A,gsyn,vsyn,Nlabels)
%This function computes the synaptic current over the axo-soma compartment
%of a postsynaptic pyramidal cell
%               _______________________________
%              |                               |
%      P       |                               |
%      R       |                               |
%      E       |                               |
%      -       |                               |
%      S       |                               |
%      Y       |                               |
%      N       |                               |
%      A       |                               |
%      P       |                               |
%      T       |                               |
%      I        _______________________________
%      C
%                   P O S T - S Y N A P T I C 
%
%       Therefore: SUM(A,1) is the number of synaptic contacts of each
%       cell. Of course, A(j,j) = 0.
%
Xlab = zeros(length(Nlabels),1);
Xlab(Nlabels==1) = 1;
Xlab1 = zeros(length(Nlabels),1);
Xlab1(Nlabels==2) = 1;

alphax = zeros(length(Nlabels),1);
alphax(Nlabels==1) = 0.0;       
alphax(Nlabels==2) = 80;         
alphax = repmat(alphax,[1,size(A,2)]);

Fpre = repmat(XH(Nlabels==1 | Nlabels==2),[1,size(A,2)]); 
Flab = repmat(Xlab(Nlabels==1 | Nlabels==2),[1,size(A,2)]);
Flabinh = repmat(Xlab1(Nlabels==1 | Nlabels==2),[1,size(A,2)]);

%Here, with Nlabels, choose who is actually pre-synaptic(!!) - Excitation
SMnmdaExc = Flab(Nlabels==1,:).*(Fpre(Nlabels==1,:).*A(Nlabels==1,:) >= 20); %20 select cells according to adj matrix and sum-up (18)
SMnmdaExc = sum(alphax(Nlabels==1,:).*SMnmdaExc,1);

%Here, with Nlabels, choose who is actually pre-synaptic(!!) - Inhibition
SMnmdaInh = Flabinh(Nlabels==2,:).*(Fpre(Nlabels==2,:).*A(Nlabels==2,:) >= 20); %select cells according to adj matrix and sum-up (18)
SMnmdaInh = sum(alphax(Nlabels==2,:).*SMnmdaInh,1);

IsynInh = gsyn*XH3.*(XH2 - vsyn(1));
IsynExc = gsyn*XH4.*(XH2 - vsyn(2));

function [Isyn,SMnmda] = sub_syncurrentdca3(XH,XH2,XH3,A,gsyn,vsyn,Nlabels)
%This function computes the synaptic current over the dendritic compartment
%of a postsynaptic pyramidal cell
%               _______________________________
%              |                               |
%      P       |                               |
%      R       |                               |
%      E       |                               |
%      -       |                               |
%      S       |                               |
%      Y       |                               |
%      N       |                               |
%      A       |                               |
%      P       |                               |
%      T       |                               |
%      I        _______________________________
%      C
%                   P O S T - S Y N A P T I C 
%
%       Therefore: SUM(A,1) is the number of synaptic contacts of each
%       cell. Of course, A(j,j) = 0.
%

Xlab = zeros(length(Nlabels),1);
Xlab(Nlabels==1) = 1;
Xlab(Nlabels==2) = 1;

alphax = zeros(length(Nlabels),1);
alphax(Nlabels==1) = 2.5; 
alphax(Nlabels==2) = 0;
alphax = repmat(alphax,[1,size(A,2)]);

Fpre = repmat(XH,[1,size(A,2)]); 
Flab = repmat(Xlab,[1,size(A,2)]);
SMnmda = Flab(Nlabels==1,:).*(Fpre(Nlabels==1,:).*A(Nlabels==1,:) >= 20); %select cells according to adj matrix and sum-up
SMnmda = sum(alphax(Nlabels==1,:).*SMnmda,1);
Isyn = gsyn*XH3.*(XH2 - vsyn(2));