function [Time,Xs,Xd,ITOT,ISYN] = simCA3CA1Network(Final_Time,CI,DT,NlabelsCA3, ...
    NlabelsCA1,A_CA3,A_CA1,A_CA3CA1)
% This function simulates a CA3-CA1 network of w/ pyramidal cells and 
% perisomatic-targeting interneurons.
%
% Solution of the system of equations is done via fourth-order 
% Runge-Kutta method. 
%
%
%Inputs: 
%Final_Time: Total time of simulation (in ms)
%CI: Initial conditions (for the two populations)
%DT: Integration time step
%NlabelsCA3: Neuron labels for CA3 (1: pyramidal; 2: interneuron)
%NlabelsCA1: Neuron labels for CA1 (1: pyramidal; 2: interneuron)
%A_CA3: CA3 neuron's connectivity matrix
%A_CA1: CA1 neuron's connectivity matrix
%A_CA3CA1: CA3-CA1 neuron's connectivity matrix
%
%Outputs:
%Time: Time 
%Xs, Xd: Response of main dyn. variables (differential equations) 
%ITOT: Transmembrane currents
%ISYN: synaptic currents (AMPA and GABA-A)
%
%JF Ramirez-Villegas (c) (Feb 2016), MPI for Biological Cybernetics
%
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



if nargin<7
    fprintf('\nNot enough input arguments\n')
    help simCA3CA1Network
    return
end

%Concat labels in order to ease handling
%1: CA3-pyr; 2: CA3-int; 3: CA1-pyr; 4: CA1-int
Nlabels = [NlabelsCA3,NlabelsCA1 + 2]; 

Total_Equations = length(Nlabels); 
Last = Final_Time/DT + 1;  %Last time step
Time = DT*[0:Last-1];  %Time vector
WTS = [1 2 2 1];  %Runge-Kutta Coefficient weights
Weights = repmat(WTS,[Total_Equations,1]);  %Make into matrix for efficiency in main loop

%Input current to the CA3 somatic compartment
Id      = mvnrnd(2.3,0.01,length(nonzeros(NlabelsCA3==1))); % 2.3 nA
Id      = abs(Id);

%Input current to the CA3 somatic compartment
IsCA1       = mvnrnd(0.0,0.0*(0.01/100),length(nonzeros(NlabelsCA1==1)));
IsCA1       = abs(IsCA1);

%Coupling conductance
gc = mvnrnd(2.1,2.1*(0.1/100),length(nonzeros(NlabelsCA3==1)));
%Leak reversal potential
VL = mvnrnd(0,0.0/100,length(nonzeros(NlabelsCA3==1)));
VL1(:,1) = mvnrnd(0,1/100,length(nonzeros(NlabelsCA1==1)));
%Maximum leakage conductance
gL = mvnrnd(0.1,0.1*(0.1/100),length(nonzeros(NlabelsCA3==1)));

%Slightly different notation here because of multiple units and multiple
%equations per unit (9 eqs pyramidal + 4 eqs interneurons)
PYR0    = (zeros(Total_Equations,Last,42)); 
Kpyr0   = (zeros(Total_Equations,4,42)); 
Itot    = zeros(Total_Equations,4,1);
Itotd   = zeros(Total_Equations,4,1);
ITOT    = zeros(Total_Equations,Last,2);
ISYN    = zeros(Total_Equations,Last,3);

PyrindCA3 = Nlabels==1; %Pyramidal cells CA3
IntindCA3 = Nlabels==2; %Interneurons CA3

PyrindCA1 = Nlabels==3; %Pyramidal cells CA1
IntindCA1 = Nlabels==4; %Interneurons CA1

CI = reshape(CI,1,1,42);
PYR0(:,1,:) = repmat(CI,[length(NlabelsCA3) + length(NlabelsCA1),1,1]);  %Initial conditions here if different from zero
Wt2 = [0 .5 .5 1];  %Second set of RK weights
rkIndex = [1 1 2 3];

%Inputs of CA1 are zero
IsCA1(:,1)      = 0.*rand(length(nonzeros(NlabelsCA1==1)),1);
IsInt(:,1)      = 0.*rand(length(nonzeros(NlabelsCA1==2)),1);
for T = 2:Last;
  for rk = 1:4  %Fourth Order Runge-Kutta (main cycle)
      for eq=1:size(PYR0,3)
        XHpy0(:,eq) = PYR0(:,T-1,eq) + Kpyr0(:,rkIndex(rk),eq)*Wt2(rk);
      end
	Tme =Time(T-1) + Wt2(rk)*DT;  %Time upgrade
    %Compute main diff. equations ----------------------------------------%
    %---------------------------------------------------------------------%
    %------------- Compute CA3 -------------------------------------------%
    %Compute pyramidal cells (CA3)
    [Kpyr0(PyrindCA3,rk,1:11),Itot(PyrindCA3,rk), ...
        Itotd(PyrindCA3,rk),IAMPA(PyrindCA3,rk), ...
        IGABA(PyrindCA3,rk),IAMPAd(PyrindCA3,rk)] = simPR_CA3Networklfp(XHpy0(Nlabels==1 | Nlabels==2,:), ...
        NlabelsCA3,A_CA3,DT,'dendritic',Id,'coupling',gc,'leak_g',gL, ...
        'leak_E',VL);
    %Compute interneurons (CA3)
    [Kpyr0(IntindCA3,rk,12:21),Itot(IntindCA3,rk), ...
        Itotd(IntindCA3,rk),IAMPA(IntindCA3,rk), ...
        IGABA(IntindCA3,rk),IAMPAd(IntindCA3,rk)] = simRB_CA3Networklfp(XHpy0(Nlabels==1 | Nlabels==2,:), ...
        NlabelsCA3,A_CA3,DT);
    %---------------------------------------------------------------------%
    %------------- Compute CA1 -------------------------------------------%
    %Compute pyramidal cells (CA1)
    [Kpyr0(PyrindCA1,rk,22:32),Itot(PyrindCA1,rk), ...
        Itotd(PyrindCA1,rk),IAMPA(PyrindCA1,rk), ...
        IGABA(PyrindCA1,rk),IAMPAd(PyrindCA1,rk)] = simPR_CA1Networklfp(XHpy0,Nlabels,NlabelsCA1, ...
        A_CA1,A_CA3CA1,DT,'somatic',IsCA1(:,1),'leak_E',VL1(:,1));
    %Compute interneurons (CA1)
    [Kpyr0(IntindCA1,rk,33:42),Itot(IntindCA1,rk), ...
        Itotd(IntindCA1,rk),IAMPA(IntindCA1,rk), ...
        IGABA(IntindCA1,rk),IAMPAd(IntindCA1,rk)] = simRB_CA1Networklfp(XHpy0, ...
        Nlabels,NlabelsCA1,A_CA1,A_CA3CA1,DT,'somatic',IsInt);
    %End main diff. equations --------------------------------------------%
 end;
 for eq=1:size(PYR0,3)
	PYR0(:,T,eq) = PYR0(:,T-1,eq) + sum((Weights.*Kpyr0(:,:,eq))')'/6;
 end
    ITOT(:,T,1) = ITOT(:,T-1,1) + sum((Weights.*Itot)')'/6; 
    ITOT(:,T,2) = ITOT(:,T-1,2) + sum((Weights.*Itotd)')'/6; 
    ISYN(:,T,1) = ISYN(:,T-1,1) + sum((Weights.*IAMPA)')'/6; 
    ISYN(:,T,2) = ISYN(:,T-1,2) + sum((Weights.*IGABA)')'/6; 
    ISYN(:,T,3) = ISYN(:,T-1,3) + sum((Weights.*IAMPAd)')'/6; 
    if mod(Time(T),1)==0 %Re-drawing current
     Id = mvnrnd(2.3,0.01,length(nonzeros(Nlabels==1)));
     Id = abs(Id);
    end
end;

%Return soma, dendrite responses 
Xs(PyrindCA3,:) = PYR0(PyrindCA3,:,1);
Xs(IntindCA3,:) = PYR0(IntindCA3,:,12);
Xs(PyrindCA1,:) = PYR0(PyrindCA1,:,22);
Xs(IntindCA1,:) = PYR0(IntindCA1,:,33);

Xd(PyrindCA3,:) = PYR0(PyrindCA3,:,2);
Xd(IntindCA3,:) = PYR0(IntindCA3,:,13);
Xd(PyrindCA1,:) = PYR0(PyrindCA1,:,23);
Xd(IntindCA1,:) = PYR0(IntindCA1,:,34);