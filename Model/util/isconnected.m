function [B] = isconnected(P)
%Connected/disconnected? based on probability P (CA3-CA1
%multi-compartmental models)
%
% Syntax: [B] = isconnected(P)
%
%Inputs:
%P: probability that neurons are connected
%
%Output: 
%B: Binary output. 1 connected, 0 non-connected based on probability P
%
%JF Ramirez-Villegas (c) (May 2014), MPI for Biological Cybernetics
%
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



if nargin<1
    fprintf('\nNot enough input arguments\n')
    help isconnected
    return
end

P = round(P*1000);

X = [ones(1,P),zeros(1,1000-P)];
X = X(randperm(length(X)));

Ind = max(round(rand(1)*length(X)),1);
B = X(Ind);