function [A] = CA1_CA3Connections(centers1,centers2,arraydist,sigma)
%This function estimates the connections from CA3 neural populations to CA1
%neural populations, based on previous initialization of the neuron
%centers (i.e., their position in a linear array). Connections are made
%according to a Gaussian probability distribution w/ width 'sigma'.
%
% Syntax: [A] = CA1_CA3Connections(centers1,centers2,sigma)
%
%Inputs:
%centers1: Centers (CA3) population
%centers2: Centers (CA1) population
%arraydist: Distance between the two arrays of neurons (mm)
%sigma: Width of the Gaussian distribution (mm)
%
%Outputs: 
%A: Connectivity matrix
%
%JF Ramirez-Villegas (c) (May 2014), MPI for Biological Cybernetics
%
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



if nargin<3
    fprintf('\nNot enough input arguments\n')
    help CA1_CA3Connections
    return
end

%Estimate cell-to-cell distance
cellds = mean(diff(centers1));

%Gaussian probability distribution
f=@(d,sigma) exp(-((d).^2)/(2*sigma^2))/sqrt(2*pi*sigma^2);

%CA3 to CA1 connections
A = zeros(length(centers1),length(centers2));

for j=1:length(centers1)
    for k=1:length(centers2)
        %Estimate distnace between the two cells
        y = abs(centers1(j) - centers2(k));
        d = sqrt(y^2 + arraydist^2);
        P = f(d,sigma); 
        [B] = isconnected(P);
        A(j,k) = A(j,k) + B;
    end
end