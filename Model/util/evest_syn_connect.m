function [centers,A,ntype] = evest_syn_connect(Ncell,celldist,sigma,varargin)
%This function estimates the connectivity of arrays of neurons based on a
%Gaussian probability distribution (the array is assumed to have a single
%class of neurons. If other types are to be included, then it should be 
%indicated in the optional input arguments.
%
% Syntax: [centers,connmat,ntype] = evest_syn_connect(Ncell,celldist,sigma,varargin)
%
%Inputs:
%Ncell: Number of neurons
%celldist: Distance between neurons
%sigma: Standard deviation of the Gaussian prob. distribution (width)
%OPTIONAL INPUT ARGUMENTS:
%   - 'twoclass' - Double. Allows setting up two-classes of neurons. The
%       the input should be the proportion of cells of the second
%       class within the array [0,1].
%   - 'sigma2' - When sigma is not identical for the two classes of
%   neurons, a new value of sigma should be given for the 2nd type of
%   neuron. If not, by default sigma values are assumed identical.
%   - 'centers' - String input, either 'linear' or 'random'. When 'linear',
%   all neurons are given a center linearly, in the order they appear. When
%   'random', centers are assigned randomly.
%   - 'uniform' - Double. Puts the 2nd population to have uniform
%   connections in a specific number of stds (e.g., 3*sigma or so) 
%
%Outputs:
%centers: Neuron centers
%A: Synaptic connectivity matrix
%ntype: Neuron type
%
%In the CA3-CA1 model: Cells are separated 10 um between each other and 
%ratio between pyramidal cells and interneurons is 10:1. Sigma for CA3-CA1 
%pyramidal cells is 1 mm, while sigma for interneurons is 100 um.
%Auto-synapses are NOT allowed. 
%
%JF Ramirez-Villegas (c) (May 2014), MPI for Biological Cybernetics
%
% Report bugs/comments to Juan F. Ramirez-Villegas
% juan.ramirez-villegas -at- tuebingen.mpg.de
%
%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
%LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
%THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


if nargin<3
    fprintf('\nNot enough input arguments\n')
    help evest_syn_connect
    return
end

optcent     = 'random';
prop2       = [];
sigma2      = [];
Ufrm        = [];
penal       = [];
for karg=1:2:length(varargin)
    switch lower(varargin{karg})
        case 'twoclass'
            prop2 = varargin{karg+1};
        case 'sigma2'
            sigma2 = varargin{karg+1};
        case 'centers'
            optcent = varargin{karg+1};
        case 'uniform'
            Ufrm = varargin{karg+1};
        case 'penalty'
            penal = varargin{karg+1};
    end
end

%Generate linear by default
celldist    = celldist*(Ncell-1);
centers     = linspace(0,celldist,Ncell);

switch lower(optcent)
    case 'random'
        centers = centers(randperm(length(centers)));
end

%Assign cell label and 2nd cell types (if requested)
ntype = ones(1,length(centers)); %cell type by default
if ~isempty(prop2)
    Ncell2 = round(Ncell*prop2);
    Indx = round(linspace(Ncell2,length(ntype),Ncell2));%ceil(length(ntype)*rand(1,Ncell2));
%     Indx(Indx==0) = 1;
    ntype(Indx) = 2;
end

%Gaussian probability distribution
f=@(x,c,sigma) exp(-((x-c).^2)/(2*sigma^2))/sqrt(2*pi*sigma^2);

A = zeros(length(centers),length(centers));
%Connectivity matrix
for kcell=1:length(centers)
    for j=1:length(centers)
        if kcell~=j
            if ntype(kcell)==1
                P = f(centers(kcell),centers(j),sigma); 
                if ~isempty(penal)
                    P = min(P+penal(1),1);
                end
                [B] = isconnected(P);
                A(kcell,j) = A(kcell,j) + B;
            elseif ntype(kcell)==2
                if isempty(Ufrm)
                    P = f(centers(kcell),centers(j),sigma2); 
                    if ~isempty(penal)
                        if ntype(j)==2
                            P = min(P+penal(2),1);
                        else
                            P = min(P+penal(2),0.75);
                        end
                    end
                    [B] = isconnected(P);
                    A(kcell,j) = A(kcell,j) + B;
                else
                    D = abs(centers(kcell) - centers(j));
                    if D<=Ufrm
                        A(kcell,j) = A(kcell,j) + 1;
                    end
                end
            end
        end
    end
end