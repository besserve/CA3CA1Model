# Matlab code for the CA3-CA1 network of two-compartment Hodgkin–Huxley neurons.

## Setup
- Clone/download the content of the repository in <MyFolder>,
- start Matlab and cd to <MyFolder>/Model,
```Matlab
cd Model
```
- add '<MyFolder>/Model/util' to the path.
```Matlab
addpath([pwd,filesep,'util'])
```

## Run simulation
- Choose simulation duration TX in seconds (warning: durations above a few seconds generate large data matrices and may lead to unresponsiveness or  Memory errors),
```Matlab
TX = 1
```
- run simulation with the following line:

```Matlab
[Time,Xs,ISYN,LFPCA1,LFPCA3,Electr,Nlabels] = genCA3CA1net(TX)
```
See help of genCA3CA1net for information about the output variables. Summary time courses are plotted on Figure 1.

![Summary time courses](basicPlot.png)