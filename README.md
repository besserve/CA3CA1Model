# Simulation of hippocampal sharp-wave ripples and sequence replay in a CA3-CA1 network.

![](CodeAvatar.png)

This repository gathers code for a biophysically-realistic model of CA3 and CA1 subfield generating sharp-wave ripples and replay.
Please cite the corresponding paper:

[Dissecting the synapse- and frequency-dependent network mechanisms of in vivo hippocampal sharp wave-ripples, J. F. Ramirez-Villegas, K. F. Willeke, N. K. Logothetis and M. Besserve. Neuron 2018; 100:1016-19.](https://doi.org/10.1016/j.neuron.2018.09.041)

Matlab codes are provided in the 'Model' folder (see documentation [there](./Model/README.md)).
